@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Records</div>
                    <div class="panel-body">
                        <a href="{{ route('users.show.me') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-btn fa-user"></i> Show Profile
                        </a>

                        <br /><br />
                        <div class="table-responsive">
                        <table id="records-table" class="table table-bordered table-hover">
                            <thead>
                                <td>Date Modified</td>
                                <td>Field</td>
                                <td>Old Value</td>
                                <td>New Value</td>
                            </thead>
                            <tbody>
                            @foreach($records as $record)
                                @if(array_key_exists('field_name', $record['fields']))
                                <tr>
                                    <td> {{ $record['created_at']['date'] }}</td>
                                    <td> {{ $record['fields']['field_name'] }}</td>
                                    <td> {{ $record['fields']['old_value'] }}</td>
                                    <td> {{ $record['fields']['new_value'] }} </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
