Please follow this link to confirm your account. You will be asked to enter your given credentials at registration:
{{ URL::to('register/verify/' . $user->confirmation_code . '?email=' . $user->email) }}.
