@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Message</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>{{ $message or 'message' }}</label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ url('/login')  }}" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Ok
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
