@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Message</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>{{ $message or 'message' }}</label>
                        </div>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register/link') }}" autocomplete="off">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input id="confirmation_code" type="hidden" class="form-control" name="confirmation_code" value="{{ $confirmation_code }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input id="email" type="hidden" class="form-control" name="email" value="{{ $email }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a class="btn btn-primary" href="{{ url('/login')  }}">Ok</a>
                                    <button type="submit" class="btn btn-link ">
                                        <i class="fa fa-btn fa-envelope"></i> Resend Password Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
