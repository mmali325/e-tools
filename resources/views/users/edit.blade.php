@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register - @if($edit) Edit @else Complete @endif</div>
                    <div class="panel-body">
                        <form id="@if($edit){{'register-edit-form'}}@else{{'register-complete-form'}}@endif" class="form-horizontal" role="form" method="POST" action="@if($edit){{ route('users.update.me') }}@else{{ route('users.store.me') }}@endif" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}

                            @if($edit)
                                {{ method_field('PUT') }}
                            @endif

                            <input type="text" name="user_id" value="" hidden>

                            @if(!$edit)
                                <p class="text-center">Please complete your registration process.</p>
                            @endif

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name', $first_name) }}">

                                      @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name', $last_name ) }}">
                                    @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">Username</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username', $username) }}">

                                    @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $email) }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @if($edit)
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Only enter if you want to change" >
                                        *Should contain at least one digit, at least one lower case, at least one upper case (8 char min).
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password_confirmation" class="col-md-4 control-label">Confirm Password</label>

                                    <div class="col-md-6">
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" data-match="#password"  value ="" data-match-error="Please enter the same value again." placeholder="Only enter if you want to change">

                                         @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div>
                            @endif

                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label for="birthday" class="col-md-4 control-label">Birthday</label>
                                <div class="col-md-6">
                                    <div class="input-group date datepicker-birthdate">
                                        <input id="birthday" type="text" class="form-control" name="birthday" value="@if($edit){{ old('birthday', $birthday) }}@else{{ old('birthday') }}@endif">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                    </div>
                                    *format DD/MM/AAAA
                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>

                             <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" name="address" value="@if($edit){{ old('address', $address) }}@else{{ old('address') }}@endif">

                                     @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                                    <div class="col-md-6">
                                    <input id="phone_number" type="text" class="form-control" name="phone_number" value="@if($edit){{ old('phone_number', $phone_number) }}@else{{ old('phone_number') }}@endif">

                                    @if ($errors->has('phone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <label for="mobile_number" class="col-md-4 control-label">Mobile Number</label>

                                <div class="col-md-6">
                                    <input id="mobile_number" type="text" class="form-control" name="mobile_number" value="@if($edit){{ old('mobile_number', $mobile_number) }}@else{{ old('mobile_number') }}@endif">

                                    @if ($errors->has('mobile_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('profile_picture') ? ' has-error' : '' }}">
                                <label for="profile_picture" class="col-md-4 control-label">Photograph</label>

                                <div class="col-md-6">
                                    @if($edit and array_key_exists('profile_picture', $files))
                                        <img src="{{ $files['profile_picture']['storage_path'] }}" alt="{{ $files['profile_picture']['name'] }}" class="img-thumbnail">
                                    @endif
                                    <input id="profile_picture" type="file" name="profile_picture" accept="image/jpeg,image/gif,image/png">
                                    <span class="help-block">
                                        <strong>Allowed Formats: jpg, png, gif</strong>
                                    </span>

                                    @if ($errors->has('profile_picture'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('profile_picture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group{{ $errors->has('drivers_license') ? ' has-error' : '' }}">
                                <label for="drivers_license" class="col-md-4 control-label">Attachment - Drivers License</label>

                                <div class="col-md-6">
                                    @if($edit and array_key_exists('drivers_license', $files))
                                        <p>Current Attachment File: <a href="{{ $files['drivers_license']['storage_path'] }}">{{ $files['drivers_license']['name'] }}</a></p>
                                    @endif

                                    @if($edit)
                                        <input id="drivers_license_name" type="text" class="form-control" name="drivers_license_name" value="@if(array_key_exists('drivers_license_name', $files)){{ old('drivers_license_name', $files['drivers_license']['name']) }}@endif" placeholder="Desired Filename">
                                    @else
                                        <input id="drivers_license_name" type="text" class="form-control" name="drivers_license_name" value="{{ old('drivers_license_name') }}" placeholder="Desired Filename">
                                    @endif
                                    <input id="drivers_license" onchange="setFilename('drivers_license')" type="file" name="drivers_license" accept="image/jpeg,image/gif,image/png,application/pdf">
                                    <span class="help-block">
                                        <strong>Allowed Formats: jpg, png, gif, pdf</strong>
                                    </span>

                                    @if ($errors->has('drivers_license'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('drivers_license') }}</strong>
                                        </span>
                                    @endif

                                    @if ($errors->has('drivers_license_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('drivers_license_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('attachment_2') ? ' has-error' : '' }}">
                                <label for="drivers_license" class="col-md-4 control-label">Attachment 2 name</label>

                                <div class="col-md-6">
                                    @if($edit and array_key_exists('attachment_2', $files))
                                        <p>Current Attachment File: <a href="{{ $files['attachment_2']['storage_path'] }}">{{ $files['attachment_2']['name'] }}</a></p>
                                    @endif

                                    @if($edit)
                                        <input id="attachment_2_name" type="text" class="form-control" name="attachment_2_name" value="@if(array_key_exists('attachment_2_name', $files)){{ old('attachment_2_name', $files['attachment_2']['name']) }}@endif" placeholder="Desired Filename">
                                    @else
                                        <input id="attachment_2_name" type="text" class="form-control" name="attachment_2_name" value="{{ old('attachment_2_name') }}" placeholder="Desired Filename">
                                    @endif
                                    <input id="attachment_2" type="file" onchange="setFilename('attachment_2')" name="attachment_2" accept="image/jpeg,image/gif,image/png,application/pdf,application/msword">
                                    <span class="help-block">
                                        <strong>Allowed Formats: jpg, png, gif, pdf, doc</strong>
                                    </span>

                                    @if ($errors->has('attachment_2'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('attachment_2') }}</strong>
                                        </span>
                                    @endif

                                    @if ($errors->has('attachment_2_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('attachment_2_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('attachment_3') ? ' has-error' : '' }}">
                                <label for="drivers_license" class="col-md-4 control-label">Attachment 3 name</label>

                                <div class="col-md-6">
                                    @if($edit and array_key_exists('attachment_3', $files))
                                        <p>Current Attachment File: <a href="{{ $files['attachment_3']['storage_path'] }}">{{ $files['attachment_3']['name'] }}</a></p>
                                    @endif

                                    @if($edit)
                                        <input id="attachment_3_name" type="text" class="form-control" name="attachment_3_name" value="@if(array_key_exists('attachment_3_name', $files)){{ old('attachment_3_name', $files['attachment_3']['name']) }}@endif" placeholder="Desired Filename">
                                    @else
                                        <input id="attachment_3_name" type="text" class="form-control" name="attachment_3_name" value="{{ old('attachment_3_name') }}" placeholder="Desired Filename">
                                    @endif
                                    <input id="attachment_3" type="file" onchange="setFilename('attachment_3')" name="attachment_3" accept="image/jpeg,image/gif,image/png,application/pdf,application/msword">
                                    <span class="help-block">
                                        <strong>Allowed Formats: jpg, png, gif, pdf, doc</strong>
                                    </span>

                                    @if ($errors->has('attachment_3'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('attachment_3') }}</strong>
                                        </span>
                                    @endif

                                    @if ($errors->has('attachment_3_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('attachment_3_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    @if($edit)
                                        <a href="{{ route('users.show.me') }}" class="btn btn-default">
                                            <i class="fa fa-btn fa-user"></i> Cancel
                                        </a>
                                    @endif
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Save Profile
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    function getFilename(inputId)
    {
        return document.getElementById(inputId).value.replace(/.*[\/\\]/, '');
    }

    function setFilename(inputId)
    {
        var filename = getFilename(inputId);
        document.getElementById(inputId+'_name').value = filename;
    }
</script>