@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <p>{{ $first_name }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <p>{{ $last_name }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <p>{{ $username }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <p>{{ $email }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                <p>{{ $birthday }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <p>{{ $phone_number }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Mobile Number</label>

                            <div class="col-md-6">
                                <p>{{ $mobile_number }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <p>{{ $address }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Photograph</label>

                            <div class="col-md-6">
                                <p><a href="{{ $files['profile_picture']['storage_path'] }}">{{ $files['profile_picture']['name'] }}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Drivers License</label>

                            <div class="col-md-6">
                                <p><a href="{{ $files['drivers_license']['storage_path'] }}">{{ $files['drivers_license']['name'] }}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Attachment 2</label>

                            <div class="col-md-6">
                                <p><a href="{{ $files['attachment_2']['storage_path'] }}">{{ $files['attachment_2']['name'] }}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Attachment 3</label>

                            <div class="col-md-6">
                                <p><a href="{{ $files['attachment_3']['storage_path'] }}">{{ $files['attachment_3']['name'] }}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ route('users.edit.me') }}" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Edit Profile
                                </a>
                                <a href="{{ route('users.index.me.records') }}" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Show History
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
