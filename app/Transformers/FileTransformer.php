<?php

namespace App\Transformers;

use App\File;
use League\Fractal\TransformerAbstract;


class FileTransformer extends TransformerAbstract
{
    /** Transform method for given resource
     * @param File $file
     * @return array
     */
    public function transform(File $file)
    {
        $data = [
            'id' => $file->id,
            'name' => $file->original_name,
            'storage_path' => request()->root().$file->storage_path,
            'user_id' => $file->fileable_id,
            'created_at' => $file->created_at,
            'updated_at' => $file->updated_at,
            'attachment_codename' => $file->attachmentType->codename,
        ];

        return $data;
    }

}