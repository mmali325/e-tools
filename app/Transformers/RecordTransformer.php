<?php

namespace App\Transformers;

use App\AttachmentType;
use App\Record;
use League\Fractal\TransformerAbstract;


class RecordTransformer extends TransformerAbstract
{
    /** Transform method for given resource
     * @param Record $record
     * @return array
     */
    public function transform(Record $record)
    {
        $data = [
            "id" => $record->id,
            "user_id" => $record->user_id,
            "table" => $record->table,
            "created_at" => $record->created_at,
            "fields" => [],
        ];

        $old_data = json_decode($record->old_data, true);
        $new_data = json_decode($record->new_data, true);

        if ($record->table == 'accounts'){

            $account_data = $this->setAccountData($old_data, $new_data);

            $data = array_merge($data, $account_data);
        } elseif ($record->table == 'users'){

            $user_data = $this->setUserData($old_data, $new_data);

            $data = array_merge($data, $user_data);
        } elseif ($record->table == 'files'){

            $file_data = $this->setFileData($old_data, $new_data);

            $data = array_merge($data, $file_data);
            $attachment_type = AttachmentType::find($new_data['attachment_type_id']);
            $data['attachment_codename'] = $attachment_type->codename;
        }

        return $data;
    }

    private function setAccountData($oldData, $newData){

        $result = [];


        $birthday_old_data = explode(" ", $oldData['birthday'])[0];
        $birthday_new_data = explode(" ", $newData['birthday'])[0];
        if($birthday_old_data != $birthday_new_data) {
            $result['fields'] = [
                'field_name' => 'birthday',
                'old_value' => empty($birthday_old_data) ? null : $birthday_old_data,
                'new_value' => empty($birthday_new_data) ? null : $birthday_new_data,
            ];
        }


        if($oldData['phone_number'] != $newData['phone_number']) {
            $result['fields'] = [
                'field_name' => 'phone_number',
                'old_value' => $oldData['phone_number'],
                'new_value' => $newData['phone_number'],
            ];
        }

        if($oldData['mobile_number'] != $newData['mobile_number']) {
            $result['fields'] = [
                'field_name' => 'mobile_number',
                'old_value' => $oldData['mobile_number'],
                'new_value' => $newData['mobile_number'],
            ];
        }

        if($oldData['address'] != $newData['address']) {
            $result['fields'] = [
                'field_name' => 'address',
                'old_value' => $oldData['address'],
                'new_value' => $newData['address'],
            ];
        }

        return $result;
    }

    private function setUserData($oldData, $newData){

        $data = [];

        $data['old_data'] = [
            "first_name" => $oldData["first_name"],
            "last_name" => $oldData["last_name"],
            "username" => $oldData["username"],
            "email" => $oldData["email"],
        ];

        $data['new_data'] = [
            "first_name" => $newData['first_name'],
            "last_name" => $newData["last_name"],
            "username" => $newData["username"],
            "email" => $newData["email"],
        ];

        $result = [];

        if($oldData['first_name'] != $newData['first_name']) {
            $result['fields'] = [
                'field_name' => 'first_name',
                'old_value' => $oldData['first_name'],
                'new_value' => $newData['first_name'],
            ];
        }

        if($oldData['last_name'] != $newData['last_name']) {
            $result['fields'] = [
                'field_name' => 'last_name',
                'old_value' => $oldData['last_name'],
                'new_value' => $newData['last_name'],
            ];
        }

        if($oldData['username'] != $newData['username']) {
            $result['fields'] = [
                'field_name' => 'username',
                'old_value' => $oldData['username'],
                'new_value' => $newData['username'],
            ];
        }

        if($oldData['email'] != $newData['email']) {
            $result['fields'] = [
                'field_name' => 'email',
                'old_value' => $oldData['email'],
                'new_value' => $newData['email'],
            ];
        }

        return $result;
    }

    private function setFileData($oldData, $newData){

        $data = [];

        $data['old_data'] = [
            "name" => $oldData["original_name"],
            "storage_path" => $oldData["storage_path"],
        ];

        $data['new_data'] = [
            "name" => $newData['original_name'],
            "storage_path" => $newData["storage_path"],
        ];

        $result = [];

        if($oldData['original_name'] != $newData['original_name']) {
            $result['fields'] = [
                'field_name' => 'original_name',
                'old_value' => $oldData['original_name'],
                'new_value' => $newData['original_name'],
            ];
        }

        if($oldData['storage_path'] != $newData['storage_path']) {
            $result['fields'] = [
                'field_name' => 'storage_path',
                'old_value' => $oldData['storage_path'],
                'new_value' => $newData['storage_path'],
            ];
        }

        return $result;
    }

}