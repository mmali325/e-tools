<?php

namespace App\Transformers;

use App\Account;
use League\Fractal\TransformerAbstract;


class AccountTransformer extends TransformerAbstract
{
    /** Transform method for given resource
     * @param Account $account
     * @return array
     */
    public function transform(Account $account)
    {
        $data = [
            'id' => $account->id,
            'birthday' => date('d/m/Y', strtotime($account->birthday)),
            'phone_number' => $account->phone_number,
            'mobile_number' => $account->mobile_number,
            'address' => $account->address,
            'user_id' => $account->user_id,
            'created_at' => $account->created_at,
            'updated_at' => $account->updated_at,
        ];

        return $data;
    }

}