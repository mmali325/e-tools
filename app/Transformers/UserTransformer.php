<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;


class UserTransformer extends TransformerAbstract
{
    /** Transform method for given resource
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        if($user->account === null and $user->files->isEmpty() === true){
            $is_complete = false;
        } else {
            $is_complete = true;
        }

        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
            'is_complete' => $is_complete,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];

        return $data;
    }

}