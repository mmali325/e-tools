<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mime extends Model
{
    /**
     * Get all episodes for this show.
     */
    public function attachmentTypes()
    {
        return $this->belongsToMany('App\AttachmentType');
    }
}
