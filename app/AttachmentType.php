<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttachmentType extends Model
{
    /**
     * Get all mimes for this attachment type.
     */
    public function mimes()
    {
        return $this->belongsToMany('App\Mime');
    }
}
