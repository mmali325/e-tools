<?php

namespace App\Http\Controllers\Api;

use App\Account;
use App\Traits\RecordTrait;
use App\User;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use App\Transformers\AccountTransformer;

use App\Http\Requests;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class AccountController extends ApiController
{
    use RecordTrait;

    protected $user;

    protected $account;

    public function __construct(Account $account, User $user) {
        $this->account = $account;
        $this->user = $user;
    }

    protected $validationRules = [
        'birthday' => 'required|string|date_format:d/m/Y',
        'phone_number' => 'required|string',
        'mobile_number' => 'required|string',
        'address'     => 'required|string',
        'user_id'     => 'present|integer',
    ];

    /**
     * @api {post} /accounts/me Add my Account
     * @apiName PostAccount
     * @apiGroup Account
     * @apiParam {String} birthday The Date of Birth for the User in format dd/mm/YY.
     * @apiParam {String} phone_number The phone number for the User with integers only.
     * @apiParam {String} mobile_number The mobile number for the User with integers only.
     * @apiParam {String} address The full address for the User.
     * @apiParam {Null} user_id The user id for the Login User. Equals null for this request.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "birthday": "21/09/1986",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": null
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (Created 201) {Date} birthday Date of Birth of Login User.
     * @apiSuccess (Created 201) {String} phone_number Phone number of the Login User.
     * @apiSuccess (Created 201) {String} mobile_number Mobile number of the Login User.
     * @apiSuccess (Created 201) {String} address Address of the Login User.
     * @apiSuccess (Created 201) {Integer} user_id ID of the Login User.
     * @apiSuccess (Created 201) {Timestamp} upadted_at Date updated of the Account instance.
     * @apiSuccess (Created 201) {Timestamp} created_at Date created of the Account instance.
     * @apiSuccess (Created 201) {Integer} id Id of the new Account.
     *
     * @apiSuccessExample Created-Response:
     *     HTTP/1.1 201 Created
     *     {
     *       "birthday": "1986-09-21 21:39:08",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": 1,
     *       "updated_at": "2016-07-29 21:39:08",
     *       "created_at": "2016-07-29 21:39:08",
     *       "id": 2
     *     }
     *
     * @apiError 422 One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "birthday": [
     *         "The birthday field is required."
     *       ],
     *       "phone_number": [
     *         "The phone number field is required."
     *       ],
     *       "mobile_number": [
     *         "The mobile number field is required."
     *       ],
     *       "address": [
     *         "The address field is required."
     *       ],
     *       "user_id": [
     *         "The user id field must be present."
     *       ]
     *     }
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $user_id = $request->get('user_id');

        if (!empty($user_id)) {

            return $this->respondNotFound(); // request for given user_id

        } else {

            $user_id = (int) Authorizer::getResourceOwnerId();

        }

        $date = $this->dateConverter($request->get('birthday'));

        $user = $this->user->findOrFail($user_id);

        $account = $user->account()->create([
            'birthday' => $date,
            'phone_number' => $request->get('phone_number'),
            'mobile_number' => $request->get('mobile_number'),
            'address' => $request->get('address'),
        ]);

        $newAccount = json_encode($account);

        $this->recordCreate($user->id, "accounts", $newAccount);

        $location = route('api.v1.accounts.show.me');

        return $this->respondCreated($account, $location);
    }

    /**
     * @api {get} /accounts/me Get my Account
     * @apiName GetAccount
     * @apiGroup Account
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *     }
     *
     * @apiSuccess (No Content 204) {Null} null User exists but has no Account related.
     *
     * @apiSuccessExample No Content-Response:
     *     HTTP/1.1 204 No Content
     *
     * @apiSuccess (OK 200) {Integer} id Id of the Account.
     * @apiSuccess (OK 200) {String} birthday Date of birth of the Login User.
     * @apiSuccess (OK 200) {String} phone_number Phone Number of the Login User.
     * @apiSuccess (OK 200) {String} mobile_number Mobile Number of the Login User.
     * @apiSuccess (OK 200) {String} address Full Address of the Login User.
     * @apiSuccess (OK 200) {Integer} user_id Id of the Login User.
     * @apiSuccess (OK 200) {Timestamp} created_at Timestamp of the created Account.
     * @apiSuccess (OK 200) {Timestamp} updated_at Timestamp of the updated Account.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 7,
     *       "birthday": "21/09/1986",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": 1,
     *       "created_at": {
     *         "date": "2016-07-29 23:00:36.000000",
     *         "timezone_type": 3,
     *         "timezone": "UTC"
     *       },
     *       "updated_at": {
     *         "date": "2016-07-29 23:00:36.000000",
     *         "timezone_type": 3,
     *         "timezone": "UTC"
     *       }
     *     }
     *
     * @apiError 401 The OAuth2 <code>access_token</code> has expired.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "access_denied",
     *       "error_description": "The resource owner or authorization server denied the request."
     *     }
     */
    public function show()
    {
        if (ends_with(request()->route()->getName(), 'accounts.show.me')){

            $user = $this->user->where('id', (int) Authorizer::getResourceOwnerId())->with('account')->firstOrFail();

        } else {

            return $this->respondNotFound();

        };

        $fractal = new Manager();

        $accountTransformer = new AccountTransformer();

        if(empty($user->account)) {
            return $this->respondNoContent();
        }

        $item = new Item($user->account, $accountTransformer);

        $data = current($fractal->createData($item)->toArray());

        return $this->respondOk($data);
    }

    /**
     * @api {put} /accounts/me Update my Account
     * @apiName UpdateAccount
     * @apiGroup Account
     * @apiParam {String} birthday The Date of Birth for the User in format dd/mm/YY.
     * @apiParam {String} phone_number The phone number for the User with integers only.
     * @apiParam {String} mobile_number The mobile number for the User with integers only.
     * @apiParam {String} address The full address for the User.
     * @apiParam {Null} user_id The user id for the Login User. Equals null for this request.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "birthday": "21/09/1986",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": null
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (OK 200) {Date} birthday Date of Birth of Login User.
     * @apiSuccess (OK 200) {String} phone_number Phone number of the Login User.
     * @apiSuccess (OK 200) {String} mobile_number Mobile number of the Login User.
     * @apiSuccess (OK 200) {String} address Address of the Login User.
     * @apiSuccess (OK 200) {Integer} user_id ID of the Login User.
     * @apiSuccess (OK 200) {Timestamp} upadted_at Date updated of the Account instance.
     * @apiSuccess (OK 200) {Timestamp} created_at Date created of the Account instance.
     * @apiSuccess (OK 200) {Integer} id Id of the new Account.
     *
     * @apiSuccessExample Updated-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "birthday": "1986-09-21 21:39:08",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": 1,
     *       "updated_at": "2016-07-29 21:39:08",
     *       "created_at": "2016-07-29 21:39:08",
     *       "id": 2
     *     }
     *
     * @apiError 422 One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "birthday": [
     *         "The birthday field is required."
     *       ],
     *       "phone_number": [
     *         "The phone number field is required."
     *       ],
     *       "mobile_number": [
     *         "The mobile number field is required."
     *       ],
     *       "address": [
     *         "The address field is required."
     *       ],
     *       "user_id": [
     *         "The user id field must be present."
     *       ]
     *     }
     */
    public function update(Request $request, $id=null)
    {
        if (ends_with(request()->route()->getName(), 'accounts.update.me')){

            $user = $this->user->findOrFail((int) Authorizer::getResourceOwnerId());

        } else {

            return $this->respondNotFound();

        };

        $this->validate($request, $this->validationRules);

        $fields = [
            'birthday' => date_create_from_format('d/m/Y', $request->get('birthday')),
            'phone_number' => $request->get('phone_number'),
            'mobile_number' => $request->get('mobile_number'),
            'address' => $request->get('address'),
        ];

        $oldAccount = json_encode($user->account);

        // update
        $user->account->update($fields);

        $newAccount = json_encode($user->account);

        $this->recordUpdate($user->id, "accounts", $oldAccount, $newAccount);

        return $this->respondOk($user->account);
    }

    /**
     * Convert string date to DateTime instance
     * @param String $date_string
     * @return \DateTime
     */
    private function dateConverter($date_string)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $date_string);

        return $date;
    }

}
