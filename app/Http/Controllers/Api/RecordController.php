<?php

namespace App\Http\Controllers\Api;

use App\Record;
use App\Transformers\RecordTransformer;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

use App\Http\Requests;


class RecordController extends ApiController
{
    protected $record;

    public function __construct(Record $record) {
        $this->record = $record;
    }

    /**
     * @api {get} /users/me Get my Records
     * @apiName GetRecords
     * @apiGroup Record
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *     }
     *
     * @apiSuccess (No Content 204) {Null} null Records doesn't exist.
     *
     * @apiSuccessExample No Content-Response:
     *     HTTP/1.1 204 No Content
     *
     * @apiSuccess (OK 200) {Integer} id Id of the Record.
     * @apiSuccess (OK 200) {Integer} user_id Id of the Login User.
     * @apiSuccess (OK 200) {String} table Table name of the Record.
     * @apiSuccess (OK 200) {Timestamp} created_at Date created of the Record.
     * @apiSuccess (OK 200) {Json} fields The modified fields.
     * @apiSuccess (OK 200) {String} field_name The name of the modified field.
     * @apiSuccess (OK 200) {String} old_value The old value of the modified field.
     * @apiSuccess (OK 200) {String} new_value The new value of the modified field.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 11,
     *         "user_id": 5,
     *         "table": "users",
     *         "created_at": {
     *           "date": "2016-07-31 01:15:08.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "fields": {
     *           "field_name": "email",
     *           "old_value": null,
     *           "new_value": "carlos.robles@copyleft.mx"
     *         }
     *       },
     *       {
     *         "id": 12,
     *         "user_id": 5,
     *         "table": "users",
     *         "created_at": {
     *           "date": "2016-07-31 01:26:57.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "fields": {
     *           "field_name": "first_name",
     *           "old_value": "Carlos",
     *           "new_value": "Carlos editado"
     *         }
     *       }
     *     ]
     *
     * @apiError 401 The OAuth2 <code>access_token</code> has expired.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "access_denied",
     *       "error_description": "The resource owner or authorization server denied the request."
     *     }
     */
    public function index()
    {
        if (ends_with(request()->route()->getName(), 'records.index.me')){

            $records = $this->record->where('user_id', (int) Authorizer::getResourceOwnerId())->get();

        } else {

            return $this->respondNotFound();

        };

        $fractal = new Manager();

        $recordTransformer = new RecordTransformer();

        $collection = new Collection($records, $recordTransformer);

        $data = current($fractal->createData($collection)->toArray());

        return $this->respondOk($data);
    }
}
