<?php

namespace App\Http\Controllers\Api;

use LucaDegasperi\OAuth2Server\Authorizer;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Issue a new access token.
     *
     * @param \LucaDegasperi\OAuth2Server\Authorizer $authorizer
     * @return \Illuminate\Http\JsonResponse
     */
    public function access_token(Authorizer $authorizer)
    {
        return response()->json($authorizer->issueAccessToken());
    }
}
