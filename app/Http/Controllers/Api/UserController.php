<?php

namespace App\Http\Controllers\Api;

use App\Traits\RecordTrait;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use App\Transformers\UserTransformer;
use App\Jobs\SendConfirmationEmail;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class UserController extends ApiController
{
    use RecordTrait;

    /**
     * @var User Model instance for User
     */
    protected $user;

    /**
     * @var array Validation rules for the resource
     */
    protected $validationRules = [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'username' => 'required|string',
        'email'     => 'required|email',
        'password'     => 'required|min:8|confirmed',
    ];

    /**
     * Constructor for UserController.
     *
     * @param User $user
     */
    function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @api {post} /users Add User
     * @apiName PostUser
     * @apiGroup User
     * @apiParam {String} first_name The Firstname for the User.
     * @apiParam {String} last_name The Lastname for the User.
     * @apiParam {String} email The email for the User.
     * @apiParam {String} username The username for the User.
     * @apiParam {String} password The password for the User.
     * @apiParam {String} password_confirmation The password confirmation for the User.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "first_name": "Carlos",
     *       "last_name": "Robles",
     *       "email": "carlos.robles@copyleft.mx",
     *       "username": "carlos.robles",
     *       "password": "MyAdminPassword",
     *       "password_confirmation": "MyAdminPassword"
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access_token for client_credentials type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (Created 201) {String} first_name Firstname for the User.
     * @apiSuccess (Created 201) {String} last_name Lastname for the User.
     * @apiSuccess (Created 201) {String} email Email for the User.
     * @apiSuccess (Created 201) {String} username Username for the User.
     * @apiSuccess (Created 201) {String} password The Password for the User.
     * @apiSuccess (Created 201) {String} password_confirmation The Password repeated for confirmation.
     *
     * @apiSuccessExample Created-Response:
     *     HTTP/1.1 201 Created
     *     {
     *       "type": "message",
     *       "codename": "confirmation_email_sent",
     *       "description": "An email has been sent to the given email address."
     *     }
     *
     * @apiError 422 One or all of the required fields are missing: <code>first_name</code>, <code>last_name</code>, <code>username</code>, <code>email</code>, <code>password</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "first_name": [
     *         "The first name field is required."
     *       ],
     *       "last_name": [
     *         "The last name field is required."
     *       ],
     *       "username": [
     *         "The username field is required."
     *       ],
     *       "email": [
     *         "The email field is required."
     *       ],
     *       "password": [
     *         "The password field is required."
     *       ]
     *     }
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $user = $this->user->create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'confirmation_code' => Str::random(60),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $newUser = json_encode($user);

        $this->recordCreate($user->id, "users", $newUser);

        $this->sendConfirmationEmail($user);

        return $this->respondConfirmationEmailSent($user->confirmation_code, $user->email);
    }

    /**
     * @api {get} /users/me Get my User
     * @apiName GetUser
     * @apiGroup User
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *     }
     *
     * @apiSuccess (No Content 204) {Null} null User doesn't exist.
     *
     * @apiSuccessExample No Content-Response:
     *     HTTP/1.1 204 No Content
     *
     * @apiSuccess (OK 200) {Integer} id Id of the User.
     * @apiSuccess (OK 200) {String} first_name Firstname of the Login User.
     * @apiSuccess (OK 200) {String} last_name Lastname of the Login User.
     * @apiSuccess (OK 200) {String} email Email of the Login User.
     * @apiSuccess (OK 200) {String} username Username of the Login User.
     * @apiSuccess (OK 200) {Boolean} is_complete Shows true for completed registration process, false otherwise.
     * @apiSuccess (OK 200) {Timestamp} created_at Timestamp of the created User.
     * @apiSuccess (OK 200) {Timestamp} updated_at Timestamp of the updated User.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 5,
     *       "first_name": "Carlos",
     *       "last_name": "Robles",
     *       "username": "carlos.robles",
     *       "email": "carlos.robles@copyleft.mx",
     *       "is_complete": false,
     *       "created_at": {
     *         "date": "2016-07-31 01:15:08.000000",
     *         "timezone_type": 3,
     *         "timezone": "UTC"
     *       },
     *       "updated_at": {
     *         "date": "2016-07-31 01:15:19.000000",
     *         "timezone_type": 3,
     *         "timezone": "UTC"
     *       }
     *     }
     *
     * @apiError 401 The OAuth2 <code>access_token</code> has expired.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "access_denied",
     *       "error_description": "The resource owner or authorization server denied the request."
     *     }
     */
    public function show($id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.show.me')){

            $user = $this->user->findOrFail((int) Authorizer::getResourceOwnerId());

        } else {
            
            return $this->respondNotFound();

        };

        $fractal = new Manager();

        $userTransformer = new UserTransformer();

        $item = new Item($user, $userTransformer);

        $data = current($fractal->createData($item)->toArray());

        return $this->respondOk($data);

    }

    /**
     * @api {put} /users/me Update my User
     * @apiName UpdateUser
     * @apiGroup User
     * @apiParam {String} first_name The Firstname for the User.
     * @apiParam {String} last_name The Lastname for the User.
     * @apiParam {String} email The email for the User.
     * @apiParam {String} username The username for the User.
     * @apiParam {String} [password] The password for the User.
     * @apiParam {String} [password_confirmation] The password confirmation for the User. Will be required if password field is set.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "first_name": "Carlos",
     *       "last_name": "Robles",
     *       "email": "carlos.robles@copyleft.mx",
     *       "username": "carlos.robles",
     *       "password": "",
     *       "password_confirmation": ""
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (OK 200) {Integer} id Id of the User.
     * @apiSuccess (OK 200) {String} first_name Firstname of the Login User.
     * @apiSuccess (OK 200) {String} last_name Lastname of the Login User.
     * @apiSuccess (OK 200) {String} email Email of the Login User.
     * @apiSuccess (OK 200) {String} username Username of the Login User.
     * @apiSuccess (OK 200) {Boolean} is_complete Shows true for completed registration process, false otherwise.
     * @apiSuccess (OK 200) {Timestamp} created_at Timestamp of the created User.
     * @apiSuccess (OK 200) {Timestamp} updated_at Timestamp of the updated User.
     *
     * @apiSuccessExample Updated-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 5,
     *       "first_name": "Carlos editado",
     *       "last_name": "Robles",
     *       "username": "carlos.robles",
     *       "confirmed": 1,
     *       "email": "carlos.robles@copyleft.mx",
     *       "created_at": "2016-07-31 01:15:08",
     *       "updated_at": "2016-07-31 01:26:57",
     *       "deleted_at": null
     *     }
     *
     * @apiError 422 One or all of the required fields are missing: <code>first_name</code>, <code>last_name</code>, <code>username</code>, <code>address</code>, <code>user_id</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "first_name": [
     *         "The first name field is required."
     *       ],
     *       "last_name": [
     *         "The last name field is required."
     *       ],
     *       "username": [
     *         "The username field is required."
     *       ],
     *       "email": [
     *         "The email field is required."
     *       ],
     *       "password": [
     *         "The password field must be present."
     *       ]
     *     }
     */
    public function update(Request $request, $id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.update.me')){

            $user = $this->user->findOrFail((int) Authorizer::getResourceOwnerId());
            $oldUser = json_encode($user);

        } else {

            return $this->respondNotFound();

        };

        $this->validationRules['password'] = 'present|min:8|confirmed';
        $this->validationRules['email'] = 'required|email|max:255|unique:users,email,'.$user->id;

        $this->validate($request, $this->validationRules);

        $fieldsToUpdate = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
        ];

        if ($request->get('password')) {

            $fieldsToUpdate['password'] = bcrypt($request->get('password'));

        } else {

            // don't update password

        }

        $user->update($fieldsToUpdate);

        $newUser = json_encode($user);

        $this->recordUpdate($user->id, "users", $oldUser, $newUser);

        return $this->respondOk($user);
    }


    public function destroy($id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.destroy.me')){

            $user = $this->user->findOrFail((int) Authorizer::getResourceOwnerId());

        } else {

            return $this->respondNotFound();

        };

        $user->delete();

        return $this->respondNoContent();

    }

    /**
     * Send a confirmation e-mail to the given user.
     *
     * @param  User $user
     * @return void
     */
    private function sendConfirmationEmail($user)
    {
        $this->dispatch(new SendConfirmationEmail($user));
    }
}
