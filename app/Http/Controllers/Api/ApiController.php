<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ApiController extends Controller
{
    /**
     * @param $objectName string The english plural or singular model name in lowercase
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated($object, $location){

        $statusCode = JsonResponse::HTTP_CREATED;

        return new JsonResponse($object, $statusCode, ["Location" => $location]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondConfirmationEmailSent($confirmation_code, $email) {

        $statusCode = JsonResponse::HTTP_CREATED;

        $message = [
            "type" => "message",
            "codename" => "confirmation_email_sent",
            "description" => "An email has been sent to the given email address.",
            "confirmation_code" => $confirmation_code,
            "email" => $email
        ];

        return new JsonResponse($message, $statusCode);
    }

    /**
     * @param $object
     * @param $location
     * @return JsonResponse
     */
    public function respondNotFound(){

        $statusCode = JsonResponse::HTTP_NOT_FOUND;

        $message = [
            "type" => "error",
            "codename" => "not_found",
            "description" => "Resource doesn't exist.",
        ];

        return new JsonResponse($message, $statusCode);
    }

    /**
     * @param $object
     * @param $location
     * @return JsonResponse
     */
    public function respondNoContent(){

        $statusCode = JsonResponse::HTTP_NO_CONTENT;

        $message = [
            "type" => "message",
            "codename" => "no_content",
            "description" => "Resource deleted successfully.",
        ];

        return new JsonResponse($message, $statusCode);
    }

    /**
     * @param $object
     * @param $location
     * @return JsonResponse
     */
    public function respondOk($object=null){

        $statusCode = JsonResponse::HTTP_OK;

        return new JsonResponse($object, $statusCode);
    }

    /**
     * @param $object
     * @param $location
     * @return JsonResponse
     */
    public function respondNotAllowed(){

        $statusCode = JsonResponse::HTTP_FORBIDDEN;

        $message = [
            "type" => "error",
            "codename" => "not_allowed",
            "description" => "You have not permissions for this action.",
        ];

        return new JsonResponse($message, $statusCode);
    }
}
