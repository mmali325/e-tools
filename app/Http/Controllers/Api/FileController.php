<?php

namespace App\Http\Controllers\Api;

use App\AttachmentType;
use App\Traits\RecordTrait;
use App\Transformers\FileTransformer;
use App\User;
use Illuminate\Http\Request;
use App\File;
use App\Http\Requests;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Storage;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class FileController extends ApiController
{
    use RecordTrait;

    protected $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    protected $validationRules = [
        'attachment_codename' => 'required|string',
        'original_name' => 'required|string',
    ];


    /**
     * @api {get} /files/me Get my Files
     * @apiName GetFiles
     * @apiGroup File
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *     }
     *
     * @apiSuccess (OK 200) {Integer} id Id of the File.
     * @apiSuccess (OK 200) {String} name Name for the File.
     * @apiSuccess (OK 200) {String} storage_path The main path were the File is being stored.
     * @apiSuccess (OK 200) {Integer} user_id Id of the Login User.
     * @apiSuccess (OK 200) {Timestamp} created_at Date created of the File.
     * @apiSuccess (OK 200) {Timestamp} updated_at Date updated of the File.
     * @apiSuccess (OK 200) {String} attachment_codename The codename for the attachment type.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "carlosRobles.png",
     *         "storage_path": "http://api.e-tools.app/storage/images/579d5b6ad7c32_79acf35e18395d93ca9a0745fba65f8b.png",
     *         "user_id": 5,
     *         "created_at": {
     *           "date": "2016-07-31 01:59:06.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "updated_at": {
     *           "date": "2016-07-31 01:59:06.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "attachment_codename": "profile_picture"
     *       },
     *       {
     *         "id": 2,
     *         "name": "file1.png",
     *         "storage_path": "http://api.e-tools.app/storage/images/579d5b6b10d04_79acf35e18395d93ca9a0745fba65f8b.png",
     *         "user_id": 5,
     *         "created_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "updated_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "attachment_codename": "drivers_license"
     *       },
     *       {
     *         "id": 3,
     *         "name": "file2.png",
     *         "storage_path": "http://api.e-tools.app/storage/images/579d5b6b29bb2_79acf35e18395d93ca9a0745fba65f8b.png",
     *         "user_id": 5,
     *         "created_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "updated_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "attachment_codename": "attachment_2"
     *       },
     *       {
     *         "id": 4,
     *         "name": "file3.png",
     *         "storage_path": "http://api.e-tools.app/storage/images/579d5b6b461cd_79acf35e18395d93ca9a0745fba65f8b.png",
     *         "user_id": 5,
     *         "created_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "updated_at": {
     *           "date": "2016-07-31 01:59:07.000000",
     *           "timezone_type": 3,
     *           "timezone": "UTC"
     *         },
     *         "attachment_codename": "attachment_3"
     *       }
     *     ]
     *
     * @apiError 401 The OAuth2 <code>access_token</code> has expired.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "access_denied",
     *       "error_description": "The resource owner or authorization server denied the request."
     *     }
     */
    public function index()
    {
        if (ends_with(request()->route()->getName(), 'files.index.me')){

            $files = $this->file->where('fileable_id', (int) Authorizer::getResourceOwnerId())->where('fileable_type', 'App\User')->get();

        } else {

            return $this->respondNotFound();

        };

        $fractal = new Manager();

        $fileTransformer = new FileTransformer();

        $item = new Collection($files, $fileTransformer);

        $data = current($fractal->createData($item)->toArray());

        return $this->respondOk($data);
    }

    /**
     * @api {post} /files/me Add my File
     * @apiName PostFile
     * @apiGroup File
     * @apiParam {Json} file Contain the key-values for the requested file.
     * @apiParam {String} name  Hash name for the File.
     * @apiParam {String} contents  Contents for the File in base64.
     * @apiParam {String} attachment_codename  Attachment codename type for the File among these: <code>"profile_picture"</code>, <code>"drivers_license"</code>, <code>"attachment_2"</code>, <code>"attachment_3"</code>.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "file":{
     *         "name": "79acf35e18395d93ca9a0745fba65f8b.png",
     *         "contents": "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGXSURBVBgZBcHPi0xxAADwz3szfoxRGqzV7C6hVhwcJU6Ii5xxWBJFDuIitaX8FViExMVNIoe9IAeSH+vHJqsVOWBmlpmdNzPv++b5fKIcMLU5HMp2/xttLUrm5p+1bp5+DEQ5PhSz8ezs4LKypodKhs2E5v3o5JnfRLkPC7LrlbEBsY55P701Y70RX16U9h39E+XeXlh+cbWgLxXJJWp6lqibupceiN5szF6tKk+KbLVOoi3R1dNUNuvb/jiMrSxf8sCMr/oymUxHW+qXqt6pOOzp+2yJlo/m9HR05L6b1FSQbiuGDU11bX/l5sUSwbSb/qk5qFeI03jAiJqKIxZq6/nkqjreq0sV0x8LK+Me2WlASx9z2mIULRbE6ZOGQQes0BUEHcOWiuTWKUnfxent130SqSCV6olUlVTt8kW4HOXuXhs9tkZNQaJpXksiNaTn0fOwu0h67sWm+vbPGtYakiuoqGh4OJsdu9KJcpyvdm8M7a1oKNmhoGXay6fh5MRHohxw4nD3eLolL1ZD1g9T4VZ2Z6IL/wGvx8Nbuo22qgAAAABJRU5ErkJggg=="
     *       },
     *       "attachment_codename": "profile_picture",
     *       "original_name": "my own name.png"
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (Created 201) (Null) null The file was successfully uploaded.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 201 OK
     *
     * @apiError 422 One or all of the required fields are missing: <code>file</code>, <code>attachment_codename</code>, <code>original_name</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "file": [
     *         "The file field is required."
     *       ],
     *       "attachment_codename": [
     *         "The attachment codename field is required."
     *       ],
     *       "original_name": [
     *         "The original name field is required."
     *       ]
     *     }
     */
    public function store(Request $request)
    {
        if (ends_with(request()->route()->getName(), 'files.store.me')){

            $user = User::findOrFail((int) Authorizer::getResourceOwnerId());

        } else {

            return $this->respondNotFound();

        };

        $this->validate($request, $this->validationRules);

        $contents = base64_decode($request->get('file')['contents']);

        $uniqid_name = uniqid().'_'.$request->get('file')['name'];

        Storage::disk('public')->put("images/".$uniqid_name, $contents);

        $path = Storage::url("images/".$uniqid_name);

        $attachment_type = AttachmentType::where('codename', $request->get('attachment_codename'))->firstOrFail();

        $file = $this->file->create([
            'original_name' => $request->get('original_name'),
            'storage_path' => $path,
            'fileable_id' => $user->id,
            'fileable_type' => 'App\User',
            'attachment_type_id' => $attachment_type->id,
        ]);

        $newFile = json_encode($file);

        $this->recordCreate($user->id, "files", $newFile);
    }

    /**
     * @api {get} /files/me/exists Check File existance
     * @apiName CheckFileExistance
     * @apiGroup File
     * @apiParam {String} attachment_codename  Attachment codename type of the File to check.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "attachment_codename": "profile_picture"
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *     }
     *
     * @apiSuccess (OK 200) {Boolean} exists Shows true if file exists, false otherwise.
     *
     * @apiSuccessExample OK-Response:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "exists": true
     *       }
     *     ]
     *
     * @apiError 401 The OAuth2 <code>access_token</code> has expired.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "access_denied",
     *       "error_description": "The resource owner or authorization server denied the request."
     *     }
     */
    public function exists(Request $request)
    {
        if (ends_with(request()->route()->getName(), 'files.exists.me')){

            $attachment_type = AttachmentType::where('codename', $request->get('attachment_codename'))->firstOrFail();

            $exists = $this->file
                ->where('fileable_id', (int) Authorizer::getResourceOwnerId())
                ->where('fileable_type', 'App\User')
                ->where('attachment_type_id', $attachment_type->id)
                ->exists();

        } else {

            return $this->respondNotFound();

        };

        return $this->respondOk(['exists' => $exists]);
    }

    /**
     * @api {put} /files/me Update my File
     * @apiName UpdateFile
     * @apiGroup File
     * @apiParam {Json} file Contain the key-values for the requested file.
     * @apiParam {String} name  Hash name for the File.
     * @apiParam {String} contents  Contents for the File in base64.
     * @apiParam {String} attachment_codename  Attachment codename type for the File among these: <code>"profile_picture"</code>, <code>"drivers_license"</code>, <code>"attachment_2"</code>, <code>"attachment_3"</code>.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "file":{
     *         "name": "79acf35e18395d93ca9a0745fba65f8b.png",
     *         "contents": "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGXSURBVBgZBcHPi0xxAADwz3szfoxRGqzV7C6hVhwcJU6Ii5xxWBJFDuIitaX8FViExMVNIoe9IAeSH+vHJqsVOWBmlpmdNzPv++b5fKIcMLU5HMp2/xttLUrm5p+1bp5+DEQ5PhSz8ezs4LKypodKhs2E5v3o5JnfRLkPC7LrlbEBsY55P701Y70RX16U9h39E+XeXlh+cbWgLxXJJWp6lqibupceiN5szF6tKk+KbLVOoi3R1dNUNuvb/jiMrSxf8sCMr/oymUxHW+qXqt6pOOzp+2yJlo/m9HR05L6b1FSQbiuGDU11bX/l5sUSwbSb/qk5qFeI03jAiJqKIxZq6/nkqjreq0sV0x8LK+Me2WlASx9z2mIULRbE6ZOGQQes0BUEHcOWiuTWKUnfxent130SqSCV6olUlVTt8kW4HOXuXhs9tkZNQaJpXksiNaTn0fOwu0h67sWm+vbPGtYakiuoqGh4OJsdu9KJcpyvdm8M7a1oKNmhoGXay6fh5MRHohxw4nD3eLolL1ZD1g9T4VZ2Z6IL/wGvx8Nbuo22qgAAAABJRU5ErkJggg=="
     *       },
     *       "attachment_codename": "profile_picture",
     *       "original_name": "my own name.png"
     *     }
     *
     * @apiHeader {String} Authorization The OAuth2 Bearer access token for password_grant type.
     * @apiHeader {String} Accept The accept type for the request.
     * @apiHeader {String} Content-Type The content-type of the request.
     *
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Authorization": "Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce"
     *       "Accept": "application/json"
     *       "Content-Type": "application/json"
     *     }
     *
     * @apiSuccess (OK 200) {Date} birthday Date of Birth of Login User.
     * @apiSuccess (OK 200) {String} phone_number Phone number of the Login User.
     * @apiSuccess (OK 200) {String} mobile_number Mobile number of the Login User.
     * @apiSuccess (OK 200) {String} address Address of the Login User.
     * @apiSuccess (OK 200) {Integer} user_id ID of the Login User.
     * @apiSuccess (OK 200) {Timestamp} upadted_at Date updated of the Account instance.
     * @apiSuccess (OK 200) {Timestamp} created_at Date created of the Account instance.
     * @apiSuccess (OK 200) {Integer} id Id of the new Account.
     *
     * @apiSuccessExample Updated-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "birthday": "1986-09-21 21:39:08",
     *       "phone_number": "5543256",
     *       "mobile_number": "432153214",
     *       "address": "Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.",
     *       "user_id": 1,
     *       "updated_at": "2016-07-29 21:39:08",
     *       "created_at": "2016-07-29 21:39:08",
     *       "id": 2
     *     }
     *
     * @apiError 422 One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "birthday": [
     *         "The birthday field is required."
     *       ],
     *       "phone_number": [
     *         "The phone number field is required."
     *       ],
     *       "mobile_number": [
     *         "The mobile number field is required."
     *       ],
     *       "address": [
     *         "The address field is required."
     *       ],
     *       "user_id": [
     *         "The user id field must be present."
     *       ]
     *     }
     */
    public function update(Request $request, $id=null)
    {
        if (ends_with(request()->route()->getName(), 'files.update.me')){

            $attachment_type = AttachmentType::where('codename', $request->get('attachment_codename'))->first();

            $user_id = (int) Authorizer::getResourceOwnerId();

            $file = $this->file->where('fileable_id', $user_id)
                ->where('fileable_type', 'App\User')
                ->where('attachment_type_id', $attachment_type->id)
                ->first();

            $oldFile = json_encode($file);

        } else {

            return $this->respondNotFound();

        };

        $garbageDir = 'garbage/';

        $non_public_path = ltrim($file->storage_path, '/storage/');

        Storage::disk('public')->move($non_public_path, $garbageDir.$non_public_path);

        $contents = base64_decode($request->get('file')['contents']);

        $uniqid_name = uniqid().'_'.$request->get('file')['name'];

        Storage::disk('public')->put("images/".$uniqid_name, $contents);

        $path = Storage::url("images/".$uniqid_name);

        $file->update([
            'original_name' => $request->get('original_name'),
            'storage_path' => $path,
        ]);

        $newFile = json_encode($file);

        $this->recordUpdate($user_id, "files", $oldFile, $newFile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id=null)
    {
        //
    }
}
