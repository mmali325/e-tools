<?php

namespace App\Http\Controllers\Auth;

use App\User;
use League\OAuth2\Server\Exception\AccessDeniedException;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Validator;
use App\Http\Controllers\Controller;
use App\Jobs\SendConfirmationEmail;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user_response = $this->createUser($data);

        $array_user_response = json_decode($user_response->getBody(), true);

        $message = $array_user_response['description'];

        return view('messages.message', ['message' => $message]);
    }

    public function confirm($confirmation_code){

        $user = User::where('email', request()->get('email'))->where('confirmation_code', $confirmation_code)->firstOrFail();
        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();

        request()->session()->put('email_confirmed', true);

        $template_data = [
            'message' => 'Email successfully verified. Please continue to Login.',
            'authenticated' => false,
        ];

        return view('messages.message')->with($template_data);
    }

    public function login(){

        $this->validate(request(), ['email' => 'required|email|max:255', 'password' => 'required|min:8']);

        try{
            $response = $this->loginUser();
        } catch(\Exception $e) {
            if(in_array($e->getCode(), [400, 401])){
                return redirect('/login')->withErrors([
                    'error' => 'These credentials do not match our records.',
                ]);
            }
        }

        // add token to session
        $array_response = json_decode($response->getBody(), true);
        request()->session()->put('access_token', $array_response['access_token']);
        request()->session()->put('refresh_token', $array_response['refresh_token']);

        $user_response = $this->getUser();

        // add token to session
        $user = json_decode($user_response->getBody(), true);

        if ($user['is_complete']) {
            return redirect()->route('users.show.me');
        } else {
            return redirect()->route('users.create.me');
        }
    }

    private function getUser() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/users/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }

    public function showLoginForm(){

        $template_data = [
            'authenticated' => false,
        ];

        return view('auth.login')->with($template_data);
    }

    public function logout(){

        request()->session()->forget('access_token');

        return redirect('login');
    }

    public function loginUser() {
        $host = config('services.etools.url');
        $client = new Client();

        // create new token
        $access_response = $client->request('POST', $host.'/oauth/access_token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => config('services.etools.id'),
                'client_secret' => config('services.etools.secret'),
                'username' => request()->get('email'),
                'password' => request()->get('password'),
            ],
        ]);

        return $access_response;
    }

    public function register()
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                request(), $validator
            );
        }

        $user_response = $this->createUser(request()->all());

        $array_user_response = json_decode($user_response->getBody(), true);

        $message = $array_user_response['description'];
        $confirmation_code = $array_user_response['confirmation_code'];
        $email = $array_user_response['email'];
        $template_data = [
            'message' => $message,
            'authenticated' => false,
            'confirmation_code' => $confirmation_code,
            'email' => $email
        ];

        return view('messages.message')->with($template_data);
    }

    public function resend_link(Request $request) {
        $confirmation_code = $request->get('confirmation_code');
        $user = User::where('email', request()->get('email'))->where('confirmation_code', $confirmation_code)->firstOrFail();
        if ($user) {
            //Resend mail
            $this->sendConfirmationEmail($user);
        }

        $template_data = [
            'message' => "An email has been re-sent to the given email address.",
            'authenticated' => false
        ];
        return view('messages.resend')->with($template_data);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $template_data = [
            'authenticated' => false,
        ];

        return view('auth.register')->with($template_data);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCompleteRegistrationForm()
    {
        return view('auth.complete_register');
    }

    protected function createUser($data) {

        $client_token = $this->clientTokenChecker();

        $host = config('services.etools.url');
        $client = new Client();

        $user_response = $client->request('POST', $host.'/users', [
            'json' => [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
                'password_confirmation' => $data['password_confirmation'],
            ],
            'headers' => [
                'Authorization' => "Bearer $client_token",
                'Accept' => "application/json",
                //'Content-Type' => "application/json",
            ],
        ]);

        return $user_response;
    }

    protected function getClientAuthorizationToken(){

        $host = config('services.etools.url');
        $client = new Client();

        // create new token
        $access_response = $client->request('POST', $host.'/oauth/access_token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => config('services.etools.id'),
                'client_secret' => config('services.etools.secret'),
            ],
        ]);

        // add token to session
        $array_access_response = json_decode($access_response->getBody(), true);

        return $array_access_response['access_token'];
    }

    /**
     * Validate token for client_grant type or issue new one if has expired.
     *
     * @return mixed|null
     */
    protected function clientTokenChecker() {
        $client_token = request()->session()->get('client_token', false);

        if ($client_token === false) {
            $client_token = $this->getClientAuthorizationToken();

            // add token to session
            request()->session()->put('client_token', $client_token);

        } else {

            // client token exists
            try {
                Authorizer::validateAccessToken(true, $client_token);
            } catch(\Exception $e) {
                if ($e instanceof AccessDeniedException){
                    $client_token = $this->getClientAuthorizationToken();
                    // add token to session
                    request()->session()->forget('client_token');
                    request()->session()->put('client_token', $client_token);
                } else {
                    return null; // not handled exception
                }
            }

        }

        return $client_token;

    }

    /**
     * Send a confirmation e-mail to the given user.
     *
     * @param  User $user
     * @return void
     */
    private function sendConfirmationEmail($user)
    {
        $this->dispatch(new SendConfirmationEmail($user));
    }
}
