<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use GuzzleHttp\Client;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (ends_with(request()->route()->getName(), 'users.index.me.records')){

            try{
                $user_response = $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

            $user = json_decode($user_response->getBody(), true);

        } else {

            return $this->respondNotFound();

        };

        if ($user['is_complete'] === false){
            return redirect()->route('users.edit.me');
        } else {
            // continue
        }

        $records_response = $this->getRecords();

        $records = json_decode($records_response->getBody(), true);

        $template_data = [
            "records" => $records,
            "authenticated" => true,
            "email" => $user['email'],
        ];

        return view('records.index')->with($template_data);
    }

    private function getRecords() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/records/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }

    private function getUser() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/users/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }
}
