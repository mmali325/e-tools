<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Requests;

class UserController extends Controller
{
    protected $validationRules = [
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'username' => 'required|max:255',
        'email' => 'required|email|max:255',
        'birthday' => 'required|string|date_format:d/m/Y',
        'phone_number' => 'required|string',
        'mobile_number' => 'required|string',
        'address'     => 'required|string',
        'user_id'     => 'present|integer',
        'profile_picture'     => 'required|mimes:gif,jpeg,png',
        'drivers_license'     => 'required|mimes:gif,jpeg,png,pdf',
        'attachment_2'     => 'required|mimes:gif,jpeg,png,pdf,doc',
        'attachment_3'     => 'required|mimes:gif,jpeg,png,pdf,doc',
        'drivers_license_name'     => 'required|string',
        'attachment_2_name'     => 'required|string',
        'attachment_3_name'     => 'required|string',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (ends_with(request()->route()->getName(), 'users.create.me')){

            try{
                $user_response = $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

            $authenticated = true;

            $user_data = json_decode($user_response->getBody(), true);

        } else {

            return $this->respondNotFound();

        };

        $template_data = [
            'first_name' => $user_data['first_name'],
            'last_name' => $user_data['last_name'],
            'username' => $user_data['username'],
            'email' => $user_data['email'],
            'email_confirmed' => false,
            'edit' => false,
            'authenticated' => $authenticated,
        ];

        return view('users.edit')->with($template_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (ends_with(request()->route()->getName(), 'users.store.me')){

            try{
                $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

        } else {

            return $this->respondNotFound();

        };

        $this->validate($request, $this->validationRules);

        $user_data = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'password_confirmation' => $request->get('password_confirmation'),
        ];

        $account_data = [
            'birthday' => $request->get('birthday'),
            'phone_number' => $request->get('phone_number'),
            'mobile_number' => $request->get('mobile_number'),
            'address' => $request->get('address'),
            'user_id' => $request->get('user_id'),
        ];

        $files_data = [
            'profile_picture' => [
                'name' => $request->file('profile_picture')->hashName(),
                'contents' => file_get_contents($request->file('profile_picture')),
                'attachment_codename' => 'profile_picture',
                'original_name' => $request->file('profile_picture')->getClientOriginalName(),
            ],
            'drivers_license' => [
                'name' => $request->file('drivers_license')->hashName(),
                'contents' => file_get_contents($request->file('drivers_license')),
                'attachment_codename' => 'drivers_license',
                'original_name' => $request->get('drivers_license_name'),
            ],
            'attachment_2' => [
                'name' => $request->file('attachment_2')->hashName(),
                'contents' => file_get_contents($request->file('attachment_2')),
                'attachment_codename' => 'attachment_2',
                'original_name' => $request->get('attachment_2_name'),
            ],
            'attachment_3' => [
                'name' => $request->file('attachment_3')->hashName(),
                'contents' => file_get_contents($request->file('attachment_3')),
                'attachment_codename' => 'attachment_3',
                'original_name' => $request->get('attachment_3_name'),
            ],
        ];

        $this->updateUser($user_data);
        $this->createAccount($account_data);
        $this->uploadMultipleFiles($files_data);

        return redirect()->route('users.show.me');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.show.me')){

            try{
                $user_response = $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

            $user = json_decode($user_response->getBody(), true);

        } else {

            return $this->respondNotFound();

        };

        if ($user['is_complete'] === false){
            return redirect()->route('users.edit.me');
        } else {
            // continue
        }

        $account_response = $this->getAccount();

        $account = json_decode($account_response->getBody(), true);

        $files_response = $this->getFiles();

        $files_array = json_decode($files_response->getBody(), true);

        $files = [];

        foreach($files_array as $f) {

            $files[$f['attachment_codename']] = [
                'name' => $f['name'],
                'storage_path' => $f['storage_path'],
            ];

        }

        $template_data = [
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'username' => $user['username'],
            'email' => $user['email'],
            'birthday' => $account['birthday'],
            'phone_number' => $account['phone_number'],
            'mobile_number' => $account['mobile_number'],
            'address' => $account['address'],
            'files' => $files,
            'authenticated' => true,
        ];

        return view('users.show')->with($template_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.edit.me')){

            try{
                $user_response = $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

            $user_data = json_decode($user_response->getBody(), true);

        } else {

            return $this->respondNotFound();

        };

        if($user_data['is_complete'] === false){
            return redirect()->route('users.create.me');
        } else {
            // continue
        }

        $account_response = $this->getAccount();

        $account_data = json_decode($account_response->getBody(), true);

        $files_response = $this->getFiles();

        $files_data = json_decode($files_response->getBody(), true);

        $files = [];

        foreach($files_data as $f) {
            $files[$f['attachment_codename']] = [
                'name' => $f['name'],
                'storage_path' => $f['storage_path'],
            ];
        }

        // pass user data in template
        $template_data = [
            'first_name' => $user_data['first_name'],
            'last_name' => $user_data['last_name'],
            'username' => $user_data['username'],
            'email' => $user_data['email'],
            'email_confirmed' => false,
            'birthday' => $account_data['birthday'],
            'phone_number' => $account_data['phone_number'],
            'mobile_number' => $account_data['mobile_number'],
            'address' => $account_data['address'],
            'files' => $files,
            'edit' => true,
            'authenticated' => false,
        ];

        return view('users.edit')->with($template_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        if (ends_with(request()->route()->getName(), 'users.update.me')){

            try{
                $user_response = $this->getUser();
            } catch(\Exception $e) {
                if(in_array($e->getCode(), [400, 401])){
                    return redirect('login');
                }
            }

        } else {

            return $this->respondNotFound();

        };

        $this->validationRules['password'] = 'present|min:8|confirmed';
        $this->validationRules['profile_picture'] = 'mimes:gif,jpeg,png';
        $this->validationRules['drivers_license'] = 'mimes:gif,jpeg,png,pdf';
        $this->validationRules['attachment_2'] = 'mimes:gif,jpeg,png,pdf,doc';
        $this->validationRules['attachment_3'] = 'mimes:gif,jpeg,png,pdf,doc';
        $this->validationRules['drivers_license_name'] = 'required_with:drivers_license|string';
        $this->validationRules['attachment_2_name'] = 'required_with:attachment_2|string';
        $this->validationRules['attachment_3_name'] = 'required_with:attachment_3|string';

        $this->validate($request, $this->validationRules);

        $user_data = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'password_confirmation' => $request->get('password_confirmation'),
        ];

        $account_data = [
            'birthday' => $request->get('birthday'),
            'phone_number' => $request->get('phone_number'),
            'mobile_number' => $request->get('mobile_number'),
            'address' => $request->get('address'),
            'user_id' => $request->get('user_id'),
        ];

        $files_data = [];

        if($request->file('profile_picture')){
            $files_data['profile_picture'] = [
                'name' => $request->file('profile_picture')->hashName(),
                'contents' => file_get_contents($request->file('profile_picture')),
                'attachment_codename' => 'profile_picture',
                'original_name' => $request->file('profile_picture')->getClientOriginalName(),
            ];
        }

        if($request->file('drivers_license')){
            $files_data['drivers_license'] = [
                'name' => $request->file('drivers_license')->hashName(),
                'contents' => file_get_contents($request->file('drivers_license')),
                'attachment_codename' => 'drivers_license',
                'original_name' => $request->get('drivers_license_name'),
            ];
        }
        if($request->file('attachment_2')){
            $files_data['attachment_2'] = [
                'name' => $request->file('attachment_2')->hashName(),
                'contents' => file_get_contents($request->file('attachment_2')),
                'attachment_codename' => 'attachment_2',
                'original_name' => $request->get('attachment_2_name'),
            ];
        }
        if($request->file('attachment_3')){
            $files_data['attachment_3'] = [
                'name' => $request->file('attachment_3')->hashName(),
                'contents' => file_get_contents($request->file('attachment_3')),
                'attachment_codename' => 'attachment_3',
                'original_name' => $request->get('attachment_3_name'),
            ];
        }

        $this->updateUser($user_data);
        $this->updateAccount($account_data);

        if(!empty($files_data)) {
            $this->uploadMultipleFiles($files_data);
        }

        return redirect()->route('users.show.me');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getUser() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/users/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }

    private function getFiles() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/files/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }

    private function uploadMultipleFiles(array $data) {

        array_map(array($this, 'uploadSingleFile'), $data);

        return true;
    }

    private function updateUser(array $data) {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('PUT', $host.'/users/me', [
            'json' => [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
                'password_confirmation' => $data['password_confirmation'],
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }

    private function createUser(array $data) {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('POST', $host.'/users/me', [
            'json' => [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
                'password_confirmation' => $data['password_confirmation'],
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }

    private function uploadSingleFile(array $data) {

        $exists_response = $this->checkFileExistance($data['attachment_codename']);

        $exists_data = json_decode($exists_response->getBody(), true);

        if ($exists_data['exists'] === true) {
            $method = 'PUT';
        } else {
            $method = 'POST';
        }

        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request($method, $host.'/files/me', [
            'json' => [
                'file' => [
                    'name' => $data['name'],
                    'contents' => base64_encode($data['contents']),
                ],
                'attachment_codename' => $data['attachment_codename'],
                'original_name' => $data['original_name'],
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }

    private function checkFileExistance($attachment_codename) {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('POST', $host.'/files/me/exists', [
            'json' => [
                'attachment_codename' => $attachment_codename,
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }

    private function getAccount() {
        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('GET', $host.'/accounts/me', [
            'headers' => [
                'Authorization' => "Bearer $access_token",
            ],
        ]);

        return $response;
    }

    private function updateAccount(array $data) {

        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('PUT', $host.'/accounts/me', [
            'json' => [
                'birthday' => $data['birthday'],
                'phone_number' => $data['phone_number'],
                'mobile_number' => $data['mobile_number'],
                'address' => $data['address'],
                'user_id' => $data['user_id'],
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }

    private function createAccount(array $data) {

        $access_token = request()->session()->get('access_token', false);

        $host = config('services.etools.url');
        $client = new Client();

        $response = $client->request('POST', $host.'/accounts/me', [
            'json' => [
                'birthday' => $data['birthday'],
                'phone_number' => $data['phone_number'],
                'mobile_number' => $data['mobile_number'],
                'address' => $data['address'],
                'user_id' => $data['user_id'],
            ],
            'headers' => [
                'Authorization' => "Bearer $access_token",
                'Accept' => "application/json",
            ],
        ]);

        return $response;
    }
}
