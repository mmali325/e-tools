<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'check.session'], function () {
    Route::auth();
    Route::get('/', function () {
        return view('welcome', ['authenticated'=>false]);
    });
    Route::get('/home', 'HomeController@index');
});

Route::get('logout', ['uses' => 'Auth\AuthController@logout']);
Route::get('register/verify/{confirmationCode}', ['as' => 'register.verify', 'uses' => 'Auth\AuthController@confirm']);
Route::post('register/link', ['as' => 'register.resendlink', 'uses' => 'Auth\AuthController@resend_link']);
Route::get('users/me', ['as' => 'users.show.me', 'uses' => 'UserController@show']);
Route::get('users/me/create', ['as' => 'users.create.me', 'uses' => 'UserController@create']);
Route::post('users/me/store', ['as' => 'users.store.me', 'uses' => 'UserController@store']);
Route::get('users/me/edit', ['as' => 'users.edit.me', 'uses' => 'UserController@edit']);
Route::put('users/me/update', ['as' => 'users.update.me', 'uses' => 'UserController@update']);
Route::get('users/me/records', ['as' => 'users.index.me.records', 'uses' => 'RecordController@index']);

Route::group(['prefix' => 'api', 'namespace' => 'Api', 'as' => 'api.', 'middleware' => 'api'], function () {

    Route::group(['prefix' => 'v1', 'as' => 'v1.'], function() {

        Route::post('oauth/access_token', ['as' => 'oauth.access_token', 'uses' => 'AuthController@access_token']);

        Route::group(['middleware' => ['oauth']], function() {

            Route::post('users', ['as' => 'users.store', 'uses' => 'UserController@store']);
            Route::get('users/me', ['as' => 'users.show.me', 'uses' => 'UserController@show']);
            Route::get('users/{users}', ['as' => 'users.show.user', 'uses' => 'UserController@show'])->where('users', '[1-9]+');
            Route::put('users/me', ['as' => 'users.update.me', 'uses' => 'UserController@update']);
            Route::put('users/{users}', ['as' => 'users.update.me', 'uses' => 'UserController@update'])->where('users', '[1-9]+');
            Route::delete('users/me', ['as' => 'users.destroy.me', 'uses' => 'UserController@destroy']);
            Route::delete('users/{users}', ['as' => 'users.destroy.user', 'uses' => 'UserController@destroy'])->where('users', '[1-9]+');

            Route::get('accounts/me', ['as' => 'accounts.show.me', 'uses' => 'AccountController@show']);
            Route::post('accounts/me', ['as' => 'accounts.store.me', 'uses' => 'AccountController@store']);
            Route::put('accounts/me', ['as' => 'accounts.update.me', 'uses' => 'AccountController@update']);
            Route::get('accounts/me/exists', ['as' => 'accounts.exists.me', 'uses' => 'AccountController@exists']);

            Route::get('files/me', ['as' => 'files.index.me', 'uses' => 'FileController@index']);
            Route::post('files/me/exists', ['as' => 'files.exists.me', 'uses' => 'FileController@exists']);
            Route::post('files/me', ['as' => 'files.store.me', 'uses' => 'FileController@store']);
            Route::put('files/me', ['as' => 'files.update.me', 'uses' => 'FileController@update']);
            Route::delete('files/me', ['as' => 'files.destroy.me', 'uses' => 'FileController@destroy']);

            Route::get('records/me', ['as' => 'records.index.me', 'uses' => 'RecordController@index']);

        });

    });

});
