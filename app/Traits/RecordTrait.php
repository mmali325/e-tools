<?php

namespace App\Traits;

use App\Record;

trait RecordTrait {

    public function recordUpdate($user_id, $tableName, $oldJSON, $newJSON){
        Record::create([
            "user_id" => $user_id,
            "table" => $tableName,
            "old_data" => $oldJSON,
            "new_data" => $newJSON,
        ]);

        return true;
    }

    public function recordCreate($user_id, $tableName, $newJSON){
        Record::create([
            "user_id" => $user_id,
            "table" => $tableName,
            "old_data" => "",
            "new_data" => $newJSON,
        ]);

        return true;
    }

}