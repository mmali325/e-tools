<?php

namespace App\OAuth2\Server\Verify;

use Illuminate\Support\Facades\Auth;

class PasswordVerify
{

    public function verify($username, $password)
    {
        $credentials = [
            'email'    => $username,
            'password' => $password,
        ];

        if (Auth::once($credentials)) {

            if (boolval(Auth::user()->confirmed) === false) {
                return false;
            } else {
                return Auth::user()->id;
            }
        }

        return false;
    }

}