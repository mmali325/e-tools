<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * The API version to use while testing the application.
     *
     * @var string
     */
    protected $version = 'v1';

    /**
     * The access token for this test.
     *
     * @var string
     */
    private $token = '5NYptI8lIp3aZsWpGBcXcXlPJagADRHTRRlVRqR4';
    /**
     * Test Login User for get valid token.
     *
     * @return \Illuminate\Foundation\Application
    */
    public function testLoginUser() {
        $headers = [
            'HTTP_Content-Type' => "application/x-www-form-urlencoded",
        ];

        $data = [
            'grant_type' => 'password',
            'client_id' => 'a46da58c75334d83c90daaf60a729023c2d136e7',
            'client_secret' => 'x1e2a1c5864c6100eccf3118db46e6f05bc18ff3',
            'username' => 'salomon.martinez+e-tools@copyleft.com.mx',
            'password' => 'Salomon1',
        ];

        $response = $this->call('POST',"/api/$this->version/oauth/access_token", $data, [], [], $headers, $data);
        //echo $response->status();
        $body = json_decode($response->content());
        $this->token = $body->access_token;

        $this->assertEquals(200, $response->status());
    }

    /**
     * Test Get User from given password type access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testGetUserMe()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $response = $this->call('GET',"/api/$this->version/users/me",[],[],[],$headers,[]);
        echo "\n\n" . $response;
        $this->assertEquals(200, $response->status());
    }

    /**
     * Test for Create Exist User from given client_type access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testAddUserIntegrityConstraintViolation()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $data = [
            'first_name' => 'Carlos',
            'last_name' => 'Robles',
            'email' => 'redocmalloc@hotmail.com',
            'username' => 'carlos.robles',
            'password' => 'admin-test',
            'password_confirmation' => 'admin-test',
        ];

        $response = $this->call('POST',"/api/$this->version/users",$data,[],[], $headers,[]);

        $this->assertEquals(500, $response->status());
    }

    /**
     * Test for Create User from given client_type access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testNewUser()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $data = [
            'first_name' => 'SA',
            'last_name' => 'MA',
            'email' => 'sama@hotmail.com',
            'username' => 'sama',
            'password' => 'unit-test',
            'password_confirmation' => 'unit-test',
        ];

        $response = $this->call('POST',"/api/$this->version/users",$data,[],[], $headers,[]);
        //echo $response;
        $this->assertEquals(200, $response->status());
    }

    /**
     * Test for Update User from given client_type access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testUpdateUser()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $data = [
            'first_name' => 'Carlos',
            'last_name' => 'Martínez',
            'email' => 'carlos.robles@copyleft.mx',
            'username' => 'carlos.robles.edited',
            'password' => '',
            'password_confirmation' => '',
        ];

        $response = $this->call('PUT',"/api/$this->version/users/me",$data,[],[], $headers,[]);
        echo $response;
        $this->assertEquals(200, $response->status());
    }

}
