<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountTest extends TestCase
{
    /**
     * The API version to use while testing the application.
     *
     * @var string
     */
    protected $version = 'v1';

    /**
     * The access token for this test.
     *
     * @var string
     */
    protected $token = 'tMpSMTLa2Ua0WatwhyWQtXVFOHlRQGkCXZMbFgP6';

    /**
     * Test Get Account from given password access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testGetMyAccount()
    {
        $response = $this->call('GET',"/api/$this->version/accounts/me",[],[],[],['HTTP_Authorization' => "Bearer $this->token"],[]);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Test for Create Account from given password access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testAddMyAccount()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $data = [
            'birthday' => '21/09/1986',
            'phone_number' => '5543256',
            'mobile_number' => '432153214',
            'address' => 'Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.',
            'user_id' => null,
        ];

        $response = $this->call('POST',"/api/$this->version/accounts/me",$data,[],[], $headers,[]);

        $this->assertEquals(201, $response->status());
    }

    /**
     * Test for Update Account from given password access token.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function testUpdateMyAccount()
    {
        $headers = [
            'HTTP_Authorization' => "Bearer $this->token",
            'HTTP_Accept' => "application/json",
            'HTTP_Content-Type' => "application/json",
        ];

        $data = [
            'birthday' => '21/09/1987',
            'phone_number' => '6666666',
            'mobile_number' => '7777777',
            'address' => 'Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.',
            'user_id' => null,
        ];

        $response = $this->call('PUT',"/api/$this->version/accounts/me",$data,[],[], $headers,[]);

        $this->assertEquals(200, $response->status());
    }

}
