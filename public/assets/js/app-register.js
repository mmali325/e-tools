
ETOOLS.validate_register_form = function(opts) {
    $.validator.addMethod("regx", function(value, element, regexpr) {
        return regexpr.test(value);
    },"Should contain at least one digit, at least one lower case, at least one upper case.");

    $.validator.addMethod("phone_au_regx", function(value, element, regexpr) {
        return regexpr.test(value);
    },"Invalid phone number. Australian format requiered.");


    $.validator.addMethod("phone", function (value, element) {
            value = jQuery.trim(value);

            // Canonicalize input
            phone = value.replace(/\D/g,'');

            if (!isValidNumber(phone, "AU")) return this.optional(element) || false;

            return this.optional(element) || true;

    }, "Invalid Phone Number");

    if (opts.mode === 'register') {
        $('#register-form').validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                username: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 8,
                    regx: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                    required: true
                },
                password_confirmation: {
                    equalTo: '#password'
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }
    if (opts.mode === 'register-complete') {
         $('#register-complete-form').validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                username: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                birthday: {
                    required: true
                },
                address: {
                    required: true
                },
                phone_number: {
                    required: true,
                    phone_au_regx: /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/

                },
                mobile_number: {
                    required: true,
                    phone_au_regx: /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/
                },
                profile_picture: {
                    required: true
                },
                drivers_license_name: {
                    required: true
                },
                drivers_license: {
                    required: true,
                },
                attachment_2_name: {
                    required: true
                },
                attachment_2: {
                    required: true
                },
                attachment_3_name: {
                    required: true
                },
                attachment_3: {
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        
        $('.datepicker-birthdate').datepicker({
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            todayHighlight: true,
            endDate: "0d"
        });
    }

    if (opts.mode === 'register-edit') {
         $('#register-edit-form').validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                username: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: false
                },
                password_confirmation: {
                    equalTo: '#password'
                },
                birthday: {
                    required: true
                },
                address: {
                    required: true
                },
                phone_number: {
                    required: true,
                    phone_au_regx: /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/
                },
                mobile_number: {
                    required: true,
                    phone_au_regx: /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/
                },
                profile_picture: {
                    required: false
                },
                drivers_license_name: {
                    required: false
                },
                drivers_license: {
                    required: false,
                },
                attachment_2_name: {
                    required: false
                },
                attachment_2: {
                    required: false
                },
                attachment_3_name: {
                    required: false
                },
                attachment_3: {
                    required: false
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        
        $('.datepicker-birthdate').datepicker({
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            todayHighlight: true,
            endDate: "0d"
        });
    }
};
