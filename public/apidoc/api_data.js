define({ "api": [
  {
    "type": "get",
    "url": "/accounts/me",
    "title": "Get my Account",
    "name": "GetAccount",
    "group": "Account",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "No Content 204": [
          {
            "group": "No Content 204",
            "type": "Null",
            "optional": false,
            "field": "null",
            "description": "<p>User exists but has no Account related.</p>"
          }
        ],
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Account.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>Date of birth of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>Phone Number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>Mobile Number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Full Address of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>Id of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Timestamp of the created Account.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Timestamp of the updated Account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "No Content-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        },
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 7,\n  \"birthday\": \"21/09/1986\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": 1,\n  \"created_at\": {\n    \"date\": \"2016-07-29 23:00:36.000000\",\n    \"timezone_type\": 3,\n    \"timezone\": \"UTC\"\n  },\n  \"updated_at\": {\n    \"date\": \"2016-07-29 23:00:36.000000\",\n    \"timezone_type\": 3,\n    \"timezone\": \"UTC\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>The OAuth2 <code>access_token</code> has expired.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"access_denied\",\n  \"error_description\": \"The resource owner or authorization server denied the request.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/AccountController.php",
    "groupTitle": "Account"
  },
  {
    "type": "post",
    "url": "/accounts/me",
    "title": "Add my Account",
    "name": "PostAccount",
    "group": "Account",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>The Date of Birth for the User in format dd/mm/YY.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>The phone number for the User with integers only.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>The mobile number for the User with integers only.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>The full address for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Null",
            "optional": false,
            "field": "user_id",
            "description": "<p>The user id for the Login User. Equals null for this request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"birthday\": \"21/09/1986\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": null\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Created 201": [
          {
            "group": "Created 201",
            "type": "Date",
            "optional": false,
            "field": "birthday",
            "description": "<p>Date of Birth of Login User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>Phone number of the Login User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>Mobile number of the Login User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the Login User.</p>"
          },
          {
            "group": "Created 201",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>ID of the Login User.</p>"
          },
          {
            "group": "Created 201",
            "type": "Timestamp",
            "optional": false,
            "field": "upadted_at",
            "description": "<p>Date updated of the Account instance.</p>"
          },
          {
            "group": "Created 201",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created of the Account instance.</p>"
          },
          {
            "group": "Created 201",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the new Account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Created-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"birthday\": \"1986-09-21 21:39:08\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": 1,\n  \"updated_at\": \"2016-07-29 21:39:08\",\n  \"created_at\": \"2016-07-29 21:39:08\",\n  \"id\": 2\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"birthday\": [\n    \"The birthday field is required.\"\n  ],\n  \"phone_number\": [\n    \"The phone number field is required.\"\n  ],\n  \"mobile_number\": [\n    \"The mobile number field is required.\"\n  ],\n  \"address\": [\n    \"The address field is required.\"\n  ],\n  \"user_id\": [\n    \"The user id field must be present.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/AccountController.php",
    "groupTitle": "Account"
  },
  {
    "type": "put",
    "url": "/accounts/me",
    "title": "Update my Account",
    "name": "UpdateAccount",
    "group": "Account",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>The Date of Birth for the User in format dd/mm/YY.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>The phone number for the User with integers only.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>The mobile number for the User with integers only.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>The full address for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Null",
            "optional": false,
            "field": "user_id",
            "description": "<p>The user id for the Login User. Equals null for this request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"birthday\": \"21/09/1986\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": null\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Date",
            "optional": false,
            "field": "birthday",
            "description": "<p>Date of Birth of Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>Phone number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>Mobile number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>ID of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "upadted_at",
            "description": "<p>Date updated of the Account instance.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created of the Account instance.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the new Account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"birthday\": \"1986-09-21 21:39:08\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": 1,\n  \"updated_at\": \"2016-07-29 21:39:08\",\n  \"created_at\": \"2016-07-29 21:39:08\",\n  \"id\": 2\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"birthday\": [\n    \"The birthday field is required.\"\n  ],\n  \"phone_number\": [\n    \"The phone number field is required.\"\n  ],\n  \"mobile_number\": [\n    \"The mobile number field is required.\"\n  ],\n  \"address\": [\n    \"The address field is required.\"\n  ],\n  \"user_id\": [\n    \"The user id field must be present.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/AccountController.php",
    "groupTitle": "Account"
  },
  {
    "type": "get",
    "url": "/files/me/exists",
    "title": "Check File existance",
    "name": "CheckFileExistance",
    "group": "File",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "attachment_codename",
            "description": "<p>Attachment codename type of the File to check.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"attachment_codename\": \"profile_picture\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Boolean",
            "optional": false,
            "field": "exists",
            "description": "<p>Shows true if file exists, false otherwise.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"exists\": true\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>The OAuth2 <code>access_token</code> has expired.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"access_denied\",\n  \"error_description\": \"The resource owner or authorization server denied the request.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/FileController.php",
    "groupTitle": "File"
  },
  {
    "type": "get",
    "url": "/files/me",
    "title": "Get my Files",
    "name": "GetFiles",
    "group": "File",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the File.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name for the File.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "storage_path",
            "description": "<p>The main path were the File is being stored.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>Id of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created of the File.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date updated of the File.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "attachment_codename",
            "description": "<p>The codename for the attachment type.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"name\": \"carlosRobles.png\",\n    \"storage_path\": \"http://api.e-tools.app/storage/images/579d5b6ad7c32_79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"user_id\": 5,\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:59:06.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"updated_at\": {\n      \"date\": \"2016-07-31 01:59:06.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"attachment_codename\": \"profile_picture\"\n  },\n  {\n    \"id\": 2,\n    \"name\": \"file1.png\",\n    \"storage_path\": \"http://api.e-tools.app/storage/images/579d5b6b10d04_79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"user_id\": 5,\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"updated_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"attachment_codename\": \"drivers_license\"\n  },\n  {\n    \"id\": 3,\n    \"name\": \"file2.png\",\n    \"storage_path\": \"http://api.e-tools.app/storage/images/579d5b6b29bb2_79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"user_id\": 5,\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"updated_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"attachment_codename\": \"attachment_2\"\n  },\n  {\n    \"id\": 4,\n    \"name\": \"file3.png\",\n    \"storage_path\": \"http://api.e-tools.app/storage/images/579d5b6b461cd_79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"user_id\": 5,\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"updated_at\": {\n      \"date\": \"2016-07-31 01:59:07.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"attachment_codename\": \"attachment_3\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>The OAuth2 <code>access_token</code> has expired.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"access_denied\",\n  \"error_description\": \"The resource owner or authorization server denied the request.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/FileController.php",
    "groupTitle": "File"
  },
  {
    "type": "post",
    "url": "/files/me",
    "title": "Add my File",
    "name": "PostFile",
    "group": "File",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "file",
            "description": "<p>Contain the key-values for the requested file.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Hash name for the File.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contents",
            "description": "<p>Contents for the File in base64.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "attachment_codename",
            "description": "<p>Attachment codename type for the File among these: <code>&quot;profile_picture&quot;</code>, <code>&quot;drivers_license&quot;</code>, <code>&quot;attachment_2&quot;</code>, <code>&quot;attachment_3&quot;</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"file\":{\n    \"name\": \"79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"contents\": \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGXSURBVBgZBcHPi0xxAADwz3szfoxRGqzV7C6hVhwcJU6Ii5xxWBJFDuIitaX8FViExMVNIoe9IAeSH+vHJqsVOWBmlpmdNzPv++b5fKIcMLU5HMp2/xttLUrm5p+1bp5+DEQ5PhSz8ezs4LKypodKhs2E5v3o5JnfRLkPC7LrlbEBsY55P701Y70RX16U9h39E+XeXlh+cbWgLxXJJWp6lqibupceiN5szF6tKk+KbLVOoi3R1dNUNuvb/jiMrSxf8sCMr/oymUxHW+qXqt6pOOzp+2yJlo/m9HR05L6b1FSQbiuGDU11bX/l5sUSwbSb/qk5qFeI03jAiJqKIxZq6/nkqjreq0sV0x8LK+Me2WlASx9z2mIULRbE6ZOGQQes0BUEHcOWiuTWKUnfxent130SqSCV6olUlVTt8kW4HOXuXhs9tkZNQaJpXksiNaTn0fOwu0h67sWm+vbPGtYakiuoqGh4OJsdu9KJcpyvdm8M7a1oKNmhoGXay6fh5MRHohxw4nD3eLolL1ZD1g9T4VZ2Z6IL/wGvx8Nbuo22qgAAAABJRU5ErkJggg==\"\n  },\n  \"attachment_codename\": \"profile_picture\",\n  \"original_name\": \"my own name.png\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Created 201) (Null": [
          {
            "group": "Created 201) (Null",
            "optional": false,
            "field": "null",
            "description": "<p>The file was successfully uploaded.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 201 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>file</code>, <code>attachment_codename</code>, <code>original_name</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"file\": [\n    \"The file field is required.\"\n  ],\n  \"attachment_codename\": [\n    \"The attachment codename field is required.\"\n  ],\n  \"original_name\": [\n    \"The original name field is required.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/FileController.php",
    "groupTitle": "File"
  },
  {
    "type": "put",
    "url": "/files/me",
    "title": "Update my File",
    "name": "UpdateFile",
    "group": "File",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "file",
            "description": "<p>Contain the key-values for the requested file.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Hash name for the File.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contents",
            "description": "<p>Contents for the File in base64.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "attachment_codename",
            "description": "<p>Attachment codename type for the File among these: <code>&quot;profile_picture&quot;</code>, <code>&quot;drivers_license&quot;</code>, <code>&quot;attachment_2&quot;</code>, <code>&quot;attachment_3&quot;</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"file\":{\n    \"name\": \"79acf35e18395d93ca9a0745fba65f8b.png\",\n    \"contents\": \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGXSURBVBgZBcHPi0xxAADwz3szfoxRGqzV7C6hVhwcJU6Ii5xxWBJFDuIitaX8FViExMVNIoe9IAeSH+vHJqsVOWBmlpmdNzPv++b5fKIcMLU5HMp2/xttLUrm5p+1bp5+DEQ5PhSz8ezs4LKypodKhs2E5v3o5JnfRLkPC7LrlbEBsY55P701Y70RX16U9h39E+XeXlh+cbWgLxXJJWp6lqibupceiN5szF6tKk+KbLVOoi3R1dNUNuvb/jiMrSxf8sCMr/oymUxHW+qXqt6pOOzp+2yJlo/m9HR05L6b1FSQbiuGDU11bX/l5sUSwbSb/qk5qFeI03jAiJqKIxZq6/nkqjreq0sV0x8LK+Me2WlASx9z2mIULRbE6ZOGQQes0BUEHcOWiuTWKUnfxent130SqSCV6olUlVTt8kW4HOXuXhs9tkZNQaJpXksiNaTn0fOwu0h67sWm+vbPGtYakiuoqGh4OJsdu9KJcpyvdm8M7a1oKNmhoGXay6fh5MRHohxw4nD3eLolL1ZD1g9T4VZ2Z6IL/wGvx8Nbuo22qgAAAABJRU5ErkJggg==\"\n  },\n  \"attachment_codename\": \"profile_picture\",\n  \"original_name\": \"my own name.png\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Date",
            "optional": false,
            "field": "birthday",
            "description": "<p>Date of Birth of Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>Phone number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "mobile_number",
            "description": "<p>Mobile number of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>ID of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "upadted_at",
            "description": "<p>Date updated of the Account instance.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created of the Account instance.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the new Account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"birthday\": \"1986-09-21 21:39:08\",\n  \"phone_number\": \"5543256\",\n  \"mobile_number\": \"432153214\",\n  \"address\": \"Plaza Melchor Ocampo 36, Cuauhtémoc, 06500 Ciudad de México, D.F.\",\n  \"user_id\": 1,\n  \"updated_at\": \"2016-07-29 21:39:08\",\n  \"created_at\": \"2016-07-29 21:39:08\",\n  \"id\": 2\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>birthday</code>, <code>phone_number</code>, <code>mobile_number</code>, <code>address</code>, <code>user_id</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"birthday\": [\n    \"The birthday field is required.\"\n  ],\n  \"phone_number\": [\n    \"The phone number field is required.\"\n  ],\n  \"mobile_number\": [\n    \"The mobile number field is required.\"\n  ],\n  \"address\": [\n    \"The address field is required.\"\n  ],\n  \"user_id\": [\n    \"The user id field must be present.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/FileController.php",
    "groupTitle": "File"
  },
  {
    "type": "get",
    "url": "/users/me",
    "title": "Get my Records",
    "name": "GetRecords",
    "group": "Record",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "No Content 204": [
          {
            "group": "No Content 204",
            "type": "Null",
            "optional": false,
            "field": "null",
            "description": "<p>Records doesn't exist.</p>"
          }
        ],
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Record.</p>"
          },
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>Id of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "table",
            "description": "<p>Table name of the Record.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created of the Record.</p>"
          },
          {
            "group": "OK 200",
            "type": "Json",
            "optional": false,
            "field": "fields",
            "description": "<p>The modified fields.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "field_name",
            "description": "<p>The name of the modified field.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "old_value",
            "description": "<p>The old value of the modified field.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "new_value",
            "description": "<p>The new value of the modified field.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "No Content-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        },
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"id\": 11,\n    \"user_id\": 5,\n    \"table\": \"users\",\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:15:08.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"fields\": {\n      \"field_name\": \"email\",\n      \"old_value\": null,\n      \"new_value\": \"carlos.robles@copyleft.mx\"\n    }\n  },\n  {\n    \"id\": 12,\n    \"user_id\": 5,\n    \"table\": \"users\",\n    \"created_at\": {\n      \"date\": \"2016-07-31 01:26:57.000000\",\n      \"timezone_type\": 3,\n      \"timezone\": \"UTC\"\n    },\n    \"fields\": {\n      \"field_name\": \"first_name\",\n      \"old_value\": \"Carlos\",\n      \"new_value\": \"Carlos editado\"\n    }\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>The OAuth2 <code>access_token</code> has expired.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"access_denied\",\n  \"error_description\": \"The resource owner or authorization server denied the request.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/RecordController.php",
    "groupTitle": "Record"
  },
  {
    "type": "get",
    "url": "/users/me",
    "title": "Get my User",
    "name": "GetUser",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "No Content 204": [
          {
            "group": "No Content 204",
            "type": "Null",
            "optional": false,
            "field": "null",
            "description": "<p>User doesn't exist.</p>"
          }
        ],
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Firstname of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Lastname of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Boolean",
            "optional": false,
            "field": "is_complete",
            "description": "<p>Shows true for completed registration process, false otherwise.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Timestamp of the created User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Timestamp of the updated User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "No Content-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        },
        {
          "title": "OK-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"first_name\": \"Carlos\",\n  \"last_name\": \"Robles\",\n  \"username\": \"carlos.robles\",\n  \"email\": \"carlos.robles@copyleft.mx\",\n  \"is_complete\": false,\n  \"created_at\": {\n    \"date\": \"2016-07-31 01:15:08.000000\",\n    \"timezone_type\": 3,\n    \"timezone\": \"UTC\"\n  },\n  \"updated_at\": {\n    \"date\": \"2016-07-31 01:15:19.000000\",\n    \"timezone_type\": 3,\n    \"timezone\": \"UTC\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>The OAuth2 <code>access_token</code> has expired.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"access_denied\",\n  \"error_description\": \"The resource owner or authorization server denied the request.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Add User",
    "name": "PostUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>The Firstname for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>The Lastname for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>The username for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The password for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": "<p>The password confirmation for the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"first_name\": \"Carlos\",\n  \"last_name\": \"Robles\",\n  \"email\": \"carlos.robles@copyleft.mx\",\n  \"username\": \"carlos.robles\",\n  \"password\": \"MyAdminPassword\",\n  \"password_confirmation\": \"MyAdminPassword\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access_token for client_credentials type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Created 201": [
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Firstname for the User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Lastname for the User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email for the User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username for the User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The Password for the User.</p>"
          },
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": "<p>The Password repeated for confirmation.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Created-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"type\": \"message\",\n  \"codename\": \"confirmation_email_sent\",\n  \"description\": \"An email has been sent to the given email address.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>first_name</code>, <code>last_name</code>, <code>username</code>, <code>email</code>, <code>password</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"first_name\": [\n    \"The first name field is required.\"\n  ],\n  \"last_name\": [\n    \"The last name field is required.\"\n  ],\n  \"username\": [\n    \"The username field is required.\"\n  ],\n  \"email\": [\n    \"The email field is required.\"\n  ],\n  \"password\": [\n    \"The password field is required.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/me",
    "title": "Update my User",
    "name": "UpdateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>The Firstname for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>The Lastname for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>The username for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": "<p>The password for the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password_confirmation",
            "description": "<p>The password confirmation for the User. Will be required if password field is set.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"first_name\": \"Carlos\",\n  \"last_name\": \"Robles\",\n  \"email\": \"carlos.robles@copyleft.mx\",\n  \"username\": \"carlos.robles\",\n  \"password\": \"\",\n  \"password_confirmation\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>The OAuth2 Bearer access token for password_grant type.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>The accept type for the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>The content-type of the request.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer yId2Xa0XEgKM4Uz7RGoMB7q9DDRDdKRcHu8aIgce\"\n  \"Accept\": \"application/json\"\n  \"Content-Type\": \"application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "OK 200": [
          {
            "group": "OK 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Firstname of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Lastname of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the Login User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Boolean",
            "optional": false,
            "field": "is_complete",
            "description": "<p>Shows true for completed registration process, false otherwise.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>Timestamp of the created User.</p>"
          },
          {
            "group": "OK 200",
            "type": "Timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Timestamp of the updated User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"first_name\": \"Carlos editado\",\n  \"last_name\": \"Robles\",\n  \"username\": \"carlos.robles\",\n  \"confirmed\": 1,\n  \"email\": \"carlos.robles@copyleft.mx\",\n  \"created_at\": \"2016-07-31 01:15:08\",\n  \"updated_at\": \"2016-07-31 01:26:57\",\n  \"deleted_at\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422",
            "description": "<p>One or all of the required fields are missing: <code>first_name</code>, <code>last_name</code>, <code>username</code>, <code>address</code>, <code>user_id</code>.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"first_name\": [\n    \"The first name field is required.\"\n  ],\n  \"last_name\": [\n    \"The last name field is required.\"\n  ],\n  \"username\": [\n    \"The username field is required.\"\n  ],\n  \"email\": [\n    \"The email field is required.\"\n  ],\n  \"password\": [\n    \"The password field must be present.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  }
] });
