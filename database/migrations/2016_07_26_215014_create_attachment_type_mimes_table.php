<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentTypeMimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachment_type_mimes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attachment_type_id')->unsigned();
            $table->foreign('attachment_type_id')->references('id')->on('attachment_types');
            $table->integer('mime_id')->unsigned();
            $table->foreign('mime_id')->references('id')->on('mimes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachment_type_mimes');
    }
}
