<?php

use Illuminate\Database\Seeder;

class AttachmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\AttachmentType::create([
            'codename' => 'profile_picture',
        ]);
        App\AttachmentType::create([
            'codename' => 'drivers_license',
        ]);
        App\AttachmentType::create([
            'codename' => 'attachment_2',
        ]);
        App\AttachmentType::create([
            'codename' => 'attachment_3',
        ]);
    }
}
