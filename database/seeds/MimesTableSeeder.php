<?php

use Illuminate\Database\Seeder;

class MimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Mime::create([
            'name' => 'PNG',
            'codename' => 'png',
        ]);
        App\Mime::create([
            'name' => 'GIF',
            'codename' => 'gif',
        ]);
        App\Mime::create([
            'name' => 'JPEG',
            'codename' => 'jpeg',
        ]);
        App\Mime::create([
            'name' => 'PDF',
            'codename' => 'pdf',
        ]);
        App\Mime::create([
            'name' => 'DOC',
            'codename' => 'doc',
        ]);
    }
}
