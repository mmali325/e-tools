<?php

use Illuminate\Database\Seeder;

class AttachmentTypeMimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attachment_types = App\AttachmentType::all();

        foreach($attachment_types as $type) {

            if($type->codename == 'profile_picture'){

                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'jpeg')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'gif')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'png')->select('id')->firstOrFail()->id,
                ]);

            } elseif ($type->codename == 'drivers_license'){

                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'jpeg')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'gif')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'png')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'pdf')->select('id')->firstOrFail()->id,
                ]);

            } elseif ($type->codename == 'attachment_2'){

                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'jpeg')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'gif')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'png')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'pdf')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'doc')->select('id')->firstOrFail()->id,
                ]);

            } elseif ($type->codename == 'attachment_3'){

                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'jpeg')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'gif')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'png')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'pdf')->select('id')->firstOrFail()->id,
                ]);
                App\AttachmentTypeMime::create([
                    'attachment_type_id' => $type->id,
                    'mime_id' => App\Mime::where('codename', 'doc')->select('id')->firstOrFail()->id,
                ]);

            } else {
                // not known type
            }
        }
    }
}
